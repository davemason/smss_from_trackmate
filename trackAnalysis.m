%-- Calculating sMSS from Trackmate data

%--------------------------
% Introduction
%--------------------------
%   A MATLAB script to take the output of the popular Fiji plugin [Trackmate](https://imagej.net/TrackMate) 
%   and calculate the slope of the moment scaling spectrum (sMSS) for eligible tracks.

%   Based largely on the work of Ivo F Sbalzarini, specifically [https://doi.org/10.1073/pnas.0504407102] and [https://doi.org/10.1016/j.jsb.2005.06.002].

%   The script also correlates motion parameters to mean spot intensity and thus requires an "all spots" Trackmate output including intensity 
%   see [https://imagej.net/TrackMate#Extensions] for adding intensity measurements. The columns in the CSV file should have the following headers:

% Label
% ID
% TRACK_ID (*)
% QUALITY
% POSITION_X (*)
% POSITION_Y (*)
% POSITION_Z (*)
% POSITION_T (*)
% FRAME
% RADIUS
% VISIBILITY
% MEAN_INTENSITY01 (*)

%   Columns with asterisks are parsed by hardcoded position and _must_ be present in these positions. 

%--------------------------
% How it Works
%--------------------------

%   For each track: the windowed displacement is calculated for tau=1 to tau=25% of the tracklength. 
%   The moments 1 through 6 are calculated for each tau (the second moment is the MSD for this tau).
%   A double log plot of the moment versus tau is created and the slope of the linear regression is 
%   called the scaling coefficient for this moment.

%   The final plot is the moment (1 to 6) against the scaling coefficients (this is the Moment Scaling
%   Spectrum: MSS), and the slope of a linear regression gives the sMSS.

%   The sMSS characterises the motion of the particles where zero is static, 0.5 is pure brownian 
%   and 1 is ballistic. Values between 0 and 0.5 suggest restricted motion and values between 0.5
%   and 1 suggest directed motion with a Brownian component. It may be useful when considering 
%   multiple tracks to plot Diffusion Coefficient against sMSS as in Fig 2A of [https://doi.org/10.1073/pnas.0504407102].

%--------------------------
% Outputs
%--------------------------
%   There are four main outputs:
%
%   {FILENAME}_output_Data.csv
%    - A CSV file with 6 columns and one row per track. Columns are: `TrackID, Track Length (frames), Diffusion Coefficient, sMSS ,Mean Link Speed , Mean Intensity`
%   {FILENAME}_output_MSD.csv
%    - A CSV file with 3 columns and one row per tau per track: This is useful if you want to plot or further analyse the MSD of the individual tracks. Columns are: `TrackID, tau, MSD`. Code is included at the end of the script to plot an MSD curve for a selected track ID.
%   {FILENAME}_output_Intensity.png
%    - An image of two subplots showing track Intensity vs Diffusion Coefficient and Intensity vs sMSS
%   {FILENAME}_output_sMSS.png
%    - An image of a plot showing Diffusion Coefficient vs sMSS for each track.
	
%--------------------------
% Acknowledgment
%--------------------------

%   Written by Dave Mason, from the University of Liverpool Centre for Cell Imaging [http://cci.liv.ac.uk].


%% -- Start
clear all
close all
clc

%-- Do you want to show waitbars for timeconsuming parts?
showStatus=1;

%-- request file and path from user
[inFile,inPath]=uigetfile('../*.csv');

%-- Load in Trackmate Spots data
if showStatus==1;tmp_wb=waitbar(0,'Intialising');end

disp(['[' datestr(now,31),'] Initialising Script and importing data']);
if showStatus==1;tmp_wb=waitbar(1/8,tmp_wb,'Importing TrackID');end
tmp_data_id=xlsread([inPath,inFile],'c:c');
if showStatus==1;tmp_wb=waitbar(1/6,tmp_wb,'Importing X');end
tmp_data_x=xlsread([inPath,inFile],'e:e');
if showStatus==1;tmp_wb=waitbar(2/6,tmp_wb,'Importing Y');end
tmp_data_y=xlsread([inPath,inFile],'f:f');
if showStatus==1;tmp_wb=waitbar(3/6,tmp_wb,'Importing Z');end
tmp_data_z=xlsread([inPath,inFile],'g:g');
if showStatus==1;tmp_wb=waitbar(4/6,tmp_wb,'Importing Time');end
tmp_data_time=xlsread([inPath,inFile],'h:h');
if showStatus==1;tmp_wb=waitbar(5/6,tmp_wb,'Importing Intensity');end
tmp_data_int=xlsread([inPath,inFile],'l:l');

%-- 2DO: if the first row below header has a trackID of "None", concatenation will fail as the arrays will be different sizes.
%-- Fix by trimming other arrays from the top to match TRACKID array

%-- concatenate columns in expected order
if showStatus==1;tmp_wb=waitbar(6/6,tmp_wb,'Concatentating Arrays');end
data=[tmp_data_x,tmp_data_y,tmp_data_z,tmp_data_time,tmp_data_id,tmp_data_int];
if showStatus==1;close(tmp_wb);end

%-- possibly need to remove anything not in a track eg. isnan(data(:,5)). NOTE: done below when finding unique tracks

clearvars tmp_*
disp(['[' datestr(now,31),'] ... done']);

%%
disp(['[' datestr(now,31),'] Calculating MSDs and sMSS']);
minTrackLength=40;
%-- get a list of trackIDs that are not NaN
tmp_trackID_list=unique(data(~isnan(data(:,5)),5));
tmp_numTracks=size(tmp_trackID_list,1);

%-- optionally use a waitbar
if showStatus==1;tmp_wb=waitbar(0,'Intialising');end

%-- for each trackID calculate the MSD
for tmp_track_ind=1:size(tmp_trackID_list,1)
    
    %-- update waitbar every 10 tracks
    if showStatus==1 && mod(tmp_track_ind,10)==0;tmp_wb=waitbar(tmp_track_ind/tmp_numTracks,tmp_wb,['Processing Tracks: ',sprintf('%1.1f',100*(tmp_track_ind/tmp_numTracks)),'% done']);end
    
    %-- Track ID
    outData(tmp_track_ind,1)=tmp_trackID_list(tmp_track_ind);
    %-- Track Length (frames)
    outData(tmp_track_ind,2)=size(data(data(:,5)==tmp_trackID_list(tmp_track_ind),5),1);
    
    %-- Check minimum track length (REMEMBER THAT WE ONLY USE 25% of this for MSD plots)
    if outData(tmp_track_ind,2)>minTrackLength
    
    %-- pull track data including timestamp
    tmp_trackPos=data(data(:,5)==tmp_trackID_list(tmp_track_ind),1:4);
    
    %-- calculate the deltaT
    tmp_deltaT=mean(diff(tmp_trackPos(:,4)));
    
    %-- calculate the tau values for the first 25% of track length
    tmp_tauNum = floor(size(tmp_trackPos,1)*0.25);
    
    
    tmp_msd = [[tmp_deltaT:tmp_deltaT:tmp_deltaT*tmp_tauNum]',zeros(tmp_tauNum,6)]; %--7 columns: tau, moment 1, moment 2 (MSD), ... moment 6
    
    %-- calculate the MSD for each tau
    for tmp_tau = 1:tmp_tauNum
        %-- calculate diffs
        tmp_Displacements = tmp_trackPos(1+tmp_tau:end,1:3) - tmp_trackPos(1:end-tmp_tau,1:3);
        %-- calculate the euclidean displacements
        tmp_Displacements = sqrt(sum(tmp_Displacements.^2,2));
        
        
        
        %-- calculate the moments of displacement
        for tmp_moment=1:6
            tmp_msd(tmp_tau,1+tmp_moment) = mean(tmp_Displacements.^tmp_moment);
        end

    end
       
    %-- save out the MSD in case you want to plot them afterwards: TRACKID,tau,MSD
    if exist('outData_MSD','var')==0
        outData_MSD=[ones(size(tmp_msd(:,1),1),1).*tmp_trackID_list(tmp_track_ind),tmp_msd(:,1),tmp_msd(:,3)];
    else
        outData_MSD=[outData_MSD;[ones(size(tmp_msd(:,1),1),1).*tmp_trackID_list(tmp_track_ind),tmp_msd(:,1),tmp_msd(:,3)]];
    end
    
    %-- now calculate the scaling coefficients by fitting the double log plots: log(tau) vs log(MxD)
    tmp_mss = zeros(6,2); %--2 columns: D2, Scaling coefficient

    %figure(); %-- don't want to plot the scaling coefficient plots for every track
    for tmp_moment=1:6
        [tmp_fit]=fit(log10(tmp_msd(:,1)),log10(tmp_msd(:,1+tmp_moment)),'poly1');
    
        if (0) %-- don't want to plot the scaling coefficient plots for every track
        subplot(2,3,tmp_moment);
        plot(tmp_fit,log10(tmp_msd(:,1)),log10(tmp_msd(:,1+tmp_moment)),'bo')
        legend 'off'
        title(['Moment=',num2str(tmp_moment)])
        xlabel 'log_1_0(tau)'
        ylabel 'log_1_0(Mean n Displacement)'
        end
        
        %-- record the Diffusion coefficient
        
        tmp_mss(tmp_moment,1)=(1/(2*tmp_moment))*exp(tmp_fit.p2); 
        %-- record the scaling coefficient
        tmp_mss(tmp_moment,2)=tmp_fit.p1;
        
    end
    
%-- fit the MSS to get the sMSS
    tmp_fit=fit([1:6]',tmp_mss(:,2),'poly1');

%-- can plot the MSS here
    if (0)
    figure()
    plot(tmp_fit,[1:6]',tmp_mss(:,2),'bo');
    xlim([0,6.5])
    tmp_y1 = ylim;
    %-- This stops errors if both Ylim(1) and Ylim(2) are negative (which is weird but may happen)
    if tmp_y1(1)>0
        tmp_y1(1)=0;
        ylim(tmp_y1)
    end
    xlim([0,7])
    xlabel 'Moment'
    ylabel 'Scaling coefficient'
    title(['Track ',num2str(tmp_trackID_list(tmp_track_ind)),' (sMSS= ',sprintf('%.2f',tmp_fit.p1),', D2= ',num2str(tmp_mss(2,2)),')'])
    
    %-- plot pure brownian reference line where slope=0.5
    hold on
    plot([0:6],[0:6].*0.5,':','Color',[0.7 0.7 0.7])
    legend({'MSS','sMSS','Slope=0.5'},'Location','SouthEast')
    end %-- if(0)

    
    %-- Record the D2 for the track
    %outData(tmp_track_ind,3)=tmp_mss(2,1);%-- not a great estimate by log log plots. Instead use initial slope of MSD plot:
    %-- tau is at least 10 points (filter on >minTrackLength features per track earlier) so use the first 5 points
    [tmp_D2fit, tmp_D2gof]=fit(tmp_msd(1:4,1),tmp_msd(1:4,3),'poly1');
    outData(tmp_track_ind,3)=tmp_D2fit.p1/4;
    %-- Record the sMSS for the track
    outData(tmp_track_ind,4)=tmp_fit.p1;
    
    %-- record the mean link speed for the track by taking the first order mean displacements and dividing by the mean time
    %-- NOTE: you can effectively do windowed velocity by taking tmp_msd(1,x) where x>1 and dividing by x*deltaT
    outData(tmp_track_ind,5)=tmp_msd(1,2)/tmp_deltaT;
        
    end %-- Minimum track length check
end %-- each track loop
if showStatus==1;close(tmp_wb);end

%-- count and report included tracks
tmp_incl=sum(outData(outData(:,2)>minTrackLength,2));
disp(['[' datestr(now,31),'] 	found ',num2str(tmp_incl),' tracks above cutoff (',num2str(minTrackLength),' frames)']);
clearvars tmp_*
disp(['[' datestr(now,31),'] ... done']);


%% -- Plot the sMSS vs D2 graph to see spread
disp(['[' datestr(now,31),'] Plotting Diffusion Coefficient against sMSS']);

%-- check to make sure you have enough tracks to plot
if sum(outData(outData(:,2)>minTrackLength,2))>0

tmp_f0=figure();
set(gcf,'position',[200 200 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

%-- exlude anything sMSS <0 and >1
outData_subset=outData(outData(:,4)>0 & outData(:,4)<1,:);
plot(outData_subset(:,3),outData_subset(:,4),'bo','MarkerSize',5)

%-- embellish plot
ylim([0,1])
hold on
tmp_xlim=xlim;
plot(tmp_xlim,[0.5,0.5],'-','Color',[1,0,0,0.5])
xlabel 'Diffusion Coefficient (um^2/s)'
ylabel 'sMSS'
title(inFile, 'Interpreter', 'none');

%-- label sMSS
tmp_xpos=tmp_xlim(1)+0.015*(diff(xlim));
text(tmp_xpos,0.52,'Brownian','Color','r','FontSize',8)
text(tmp_xpos,0.98,'Ballistic','Color','r','FontSize',8)
text(tmp_xpos,0.02,'Static','Color','r','FontSize',8)

saveas(tmp_f0,[inPath,replace(inFile,'.csv','_output_sMSS.png')]);
clearvars tmp_*

else
    disp(['[' datestr(now,31),'] ERROR: not enough tracks to plot']);
end %-- minTracks check

disp(['[' datestr(now,31),'] ... done']);

%% -- Correlate motion characteristics with intensity
disp(['[' datestr(now,31),'] Correlating and plotting mean track intensity']);

if sum(outData(outData(:,2)>minTrackLength,2))>0
    
%-- exlude anything sMSS <0 and >1
outData_subset=outData(outData(:,4)>0 & outData(:,4)<1,:);

%-- for each track in the output, pull the mean intensity
for tmp_i=1:size(outData_subset,1)
    outData_subset(tmp_i,6)=mean(data(data(:,5)==outData_subset(tmp_i,1),6));
end

%-- plot
tmp_f1=figure();
set(gcf,'position',[200 200 1200 500]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

%-- sMSS vs intensity
subplot(1,2,1)
plot(outData_subset(:,6),outData_subset(:,4),'bo')
xlabel 'Mean Intensity (a.u.)'
ylabel 'sMSS'
title(inFile, 'Interpreter', 'none');
ylim([0,1])
hold on
tmp_xlim=xlim;
plot(tmp_xlim,[0.5,0.5],'-','Color',[1,0,0,0.5])

%-- D2 vs intensity
subplot(1,2,2)
plot(outData_subset(:,6),outData_subset(:,3),'bo')
xlabel 'Mean Intensity (a.u.)'
ylabel 'D2 (um^2/s)'
title(inFile, 'Interpreter', 'none');

saveas(tmp_f1,[inPath,replace(inFile,'.csv','_output_Intensity.png')]);
clearvars tmp_*

else
    disp(['[' datestr(now,31),'] ERROR: not enough tracks to plot']);
end %-- minTracks check

disp(['[' datestr(now,31),'] ... done']);


%% -- Saving output data table
disp(['[' datestr(now,31),'] Saving data']);

%-- output the subsetted data: TRACKID,NumSPOTS,D2,sMSS,linkSpeed,intensity
csvwrite([inPath,replace(inFile,'.csv','_output_Data.csv')],outData_subset);

%-- output the MSD data: TRACKID,tau,MSD
csvwrite([inPath,replace(inFile,'.csv','_output_MSD.csv')],outData_MSD);

clearvars tmp_*
disp(['[' datestr(now,31),'] ... done']);



%% -- Plot MSD for single track (not to be run as part of the main script)
if (0)
%-- sort the data
outData_MSD=sortrows(outData_MSD,[1,2]);

%-- List of trackIDs with MSD calculated
unique(outData_MSD(:,1))'
%-- pick a track
tmp_trackID=10

disp('---------------------------------------');
disp(['Using Track ',num2str(tmp_trackID)]);
disp(['Track length = ',num2str(size(outData_MSD(outData_MSD(:,1)==tmp_trackID,2),1)),' frames']);
figure();
set(gcf,'position',[200 200 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

plot(outData_MSD(outData_MSD(:,1)==tmp_trackID,2),outData_MSD(outData_MSD(:,1)==tmp_trackID,3),'o-')

%-- calculate the apparent Diffusion coefficient based on Kusumi et al 1993 Biophysical Journal

%-- try a linear fit (brownian motion) using MSD(dT)= 4D.dT to the first four points of the MSD plot (this should give a reasonable estimate for restricted and Brownian
tmp_data=outData_MSD(outData_MSD(:,1)==tmp_trackID,2:3);
[tmp_fit,tmp_gof]=fit(tmp_data(1:4,1),tmp_data(1:4,2),'poly1');
hold on
plot(tmp_fit,'r--');
legend 'off'
disp(['Fitting whole track for Brownian motion...']);
disp(['Apparent Diffusion Coefficient = ',sprintf('%1.2e',tmp_fit.p1/4),' um^2/s']);
disp(['R^2= ',sprintf('%1.2f',tmp_gof.rsquare)]);
disp('---------------------------------------');

xlabel 'Tau (s)'
ylabel 'MSD (um^2/s)'
title([inFile,' - Track ',num2str(tmp_trackID)],'Interpreter','none')


clearvars tmp_*

end %-- if(0)
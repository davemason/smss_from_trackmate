# Calculating sMSS from Trackmate data #
## Introduction ##
A MATLAB script to take the output of the popular Fiji plugin [Trackmate](https://imagej.net/TrackMate) and calculate the slope of the moment scaling spectrum (sMSS) for eligible tracks.

Based largely on the work of [Ivo F Sbalzarini](https://www.mpi-cbg.de/research-groups/current-groups/ivo-sbalzarini/group-leader/), specifically [this](https://doi.org/10.1073/pnas.0504407102) and [this](https://doi.org/10.1016/j.jsb.2005.06.002) paper.

The script also correlates motion parameters to mean spot intensity and thus requires an "all spots" Trackmate output including intensity (see [here](https://imagej.net/TrackMate#Extensions) for adding intensity measurements). The columns in the CSV file should be:

- Label
- ID
- TRACK_ID (*)
- QUALITY
- POSITION_X (*)
- POSITION_Y (*)
- POSITION_Z (*)
- POSITION_T (*)
- FRAME
- RADIUS
- VISIBILITY
- MEAN_INTENSITY01 (*)

Columns with asterisks are parsed by hardcoded position and must be present in these positions. 

## How it works ##

For each track: the windowed displacement is calculated for tau=1 to tau=25% of the tracklength. The moments 1 through 6 are calculated for each tau (the second moment is the MSD for this tau). A double log plot of the moment versus tau is created and the slope of the linear regression is called the scaling coefficient for this moment.

The final plot is the moment (1 to 6) against the scaling coefficients (this is the Moment Scaling Spectrum: MSS), and the slope of a linear regression gives the sMSS.

![plot](https://bitbucket.org/davemason/smss_from_trackmate/raw/master/sMSS.png)

The sMSS characterises the motion of the particles where zero is static, 0.5 is pure brownian and 1 is ballistic. Values between 0 and 0.5 suggest restricted motion and values between 0.5 and 1 suggest directed motion with a Brownian component. It may be useful when considering multiple tracks to plot Diffusion Coefficient against sMSS as in [Fig 2A here](https://doi.org/10.1073/pnas.0504407102).

## Outputs ##
There are four main outputs:

- **\{FILENAME}_output_Data.csv**
    - A CSV file with 6 columns and one row per track. Columns are: `TrackID, Track Length (frames), Diffusion Coefficient, sMSS ,Mean Link Speed , Mean Intensity`
- **\{FILENAME}_output_MSD.csv**
    - A CSV file with 3 columns and one row per tau per track: This is useful if you want to plot or further analyse the MSD of the individual tracks. Columns are: `TrackID, tau, MSD`. Code is included at the end of the script to plot an MSD curve for a selected track ID.
- **\{FILENAME}_output_Intensity.png**
    - An image of two subplots showing track Intensity vs Diffusion Coefficient and Intensity vs sMSS
- **\{FILENAME}_output_sMSS.png**
    - An image of a plot showing Diffusion Coefficient vs sMSS for each track.
	
## Acknowledgment ##

Written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk).
